$(document).ready(() => {
  $('form[name="contact"]').submit(function (event) {
    event.preventDefault();
    let valid = true;

    $('.uk-form-danger').removeClass('uk-form-danger');

    if (!$('input[name="name"]').val()) {
      $('input[name="name"]').addClass('uk-form-danger')
      valid = false;
    }

    if (!$('input[name="email"]').val()) {
      $('input[name="email"]').addClass('uk-form-danger')
      valid = false;
    }

    if (!$('input[name="subject"]').val()) {
      $('input[name="subject"]').addClass('uk-form-danger')
      valid = false;
    }

    if (!$('textarea[name="message"]').val()) {
      $('textarea[name="message"]').addClass('uk-form-danger')
      valid = false;
    }

    if (valid) {
      fetch("/", {
        method: "POST",
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        body: new URLSearchParams(new FormData(event.target)).toString(),
      }).then(() => {
        Swal.fire({ icon: "success", title: "Great Success!", text: "Message was sent!" });
        this.reset();
      }).catch((error) => {
        Swal.fire({ icon: "error", title: "Oops...", text: "Something went wrong!" });
      });
    }
  });
});